import React, { Componet } from 'react'
import {
    View, Text, TextInput, Button, Alert, StyleSheet,
    Image, TouchableWithoutFeedback, StatusBar, SafeAreaView, Keyboard, TouchableOpacity,
    KeyboardAvoidingView
} from 'react-native'

class LoginExScreen extends React.Component {
    static navigationOptions = {
        title: "Login",
    }

    state = {
        username: "",
        password: "",
    }

    checkLogin() {
        console.log('check in');
        const { username, password } = this.state
        if (username == '12' && password == '12') {
            console.log('login ok');
            this.props.navigation.navigate('Home')
        } else {
            Alert.alert('Error', 'Something wrong with id or password', [{ text: 'Okay' }])
        }
    }

    render() {
        return (
            <SafeAreaView style={styles.parent}>
                <StatusBar barStyle="light-content" />
                <KeyboardAvoidingView behavior='padding' style={styles.parent}>
                    <TouchableWithoutFeedback style={styles.parent} onPress={Keyboard.dismiss}>
                        <View style={styles.logoContainer}>
                            <View style={styles.logoContainer}>
                                <Image style={styles.logo} source={{ uri: 'https://vignette.wikia.nocookie.net/logopedia/images/e/ee/Burger_King_Logo.svg.png' }}>
                                </Image>
                                <Text style={styles.title}>Account Information</Text>
                            </View>
                            <View style={styles.infoContainer}>
                                <TextInput style={styles.input}
                                    onChangeText={text => this.setState({ username: text })}
                                    placeholder="username 12"
                                    placeholderTextColor='rgba(255,255,255,0.8)'
                                    keyboardType='email-address'
                                    autoCorrect={false}
                                    onSubmitEditing={() => this.refs.txtPassword.focus()}
                                ></TextInput>
                                <TextInput style={styles.input}
                                    secureTextEntry={true}
                                    onChangeText={text => this.setState({ password: text })}
                                    placeholder="password 12"
                                    placeholderTextColor='rgba(255,255,255,0.8)'
                                    ref={"txtPassword"}
                                ></TextInput>
                                <TouchableOpacity style={styles.buttonContainer} onPress={_ => this.checkLogin()}>
                                    <Text style={styles.buttonText}>SIGN IN</Text>
                                </TouchableOpacity>
                            </View>
                          
                        </View>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>

            </SafeAreaView>

        )
    }
}

export default LoginExScreen

styles = StyleSheet.create({
    parent: {
        flex: 1,
        backgroundColor: 'rgb(32,53,70)',
        flexDirection: 'column',
    },
    logoContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    logo: {
        width: 128,
        height: 128,
    },
    title: {
        color: '#f7c744',
        fontSize: 18,
        textAlign: 'center',
        marginTop: 5,
        opacity: 0.9
    },
    input: {
        height: 40,
        backgroundColor: 'rgba(255,255,255,0.2)',
        marginHorizontal: 10,
        marginVertical: 5,
        color: '#FFF',
        paddingHorizontal: 10,
        // borderColor: 'rgba(255,255,255,0.2)',        
    },    
    infoContainer: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        height: 200,
        // backgroundColor: 'red',
    },
    buttonContainer: {
        backgroundColor: '#f7c744',
        paddingVertical: 15,
        marginHorizontal:10,
    },
    buttonText: {
        textAlign: 'center',
        color: 'rgb(32,53,70)',
        fontSize:18,
        fontWeight:'bold',
    }
});