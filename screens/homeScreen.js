import React from 'react';
import { StyleSheet, Text, View,Button, } from 'react-native';


export default class HomeScreen extends React.Component {

    static navigationOptions = {
        header:null,
    }

    render(){
        // const {navigate} = this.props.navigation;
        return(
            <View style={styles.container}>
                <View style={styles.upperContainer}>
                    <Text>This is home Screen</Text>
                </View>
                <View style={styles.downContainer}>
                    <Button title="Calculator" onPress={() =>this.props.navigation.navigate('Calculater')}></Button>
                    <View style={{height:11}}/>
                    <Button title="LoginExample" onPress={() =>this.props.navigation.navigate('LoginEx')}></Button>
                    <View style={{height:11}}/>
                    <Button title="PaperExample" onPress={() =>this.props.navigation.navigate('PaperEx')}></Button>
                    <View style={{height:11}}/>
                    <Button title="ToDoExample" onPress={() =>this.props.navigation.navigate('ToDoEx')}></Button>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
    flex: 1,
    backgroundColor:"#ffcccc",
    paddingTop :25,
    },
    upperContainer:{
        flex:3,
        alignItems:'center',
        justifyContent:'center'
    },
    downContainer:{
        flex:7,
        alignItems:'center',
        justifyContent:'center'
    },
});