import React, { Componet } from 'react'
import { View } from 'react-native'
import { Provider as PaperProvider, Appbar, Checkbox, List, Text, TouchableRipple } from 'react-native-paper'
import { StyleSheet, SafeAreaView, Platform } from 'react-native';

class PaperUiScreen extends React.Component {
    static navigationOptions = {
        title: "Paper",
    }
    state = {
        checked: false,
        expanded: true
    };

    _handlePress = () =>
        this.setState({
            expanded: !this.state.expanded
        });


    render() {
        const { checked } = this.state;
        return (
            <SafeAreaView style={styles.safeArea}>
                <PaperProvider>
                    <Appbar style={styles.bottom}>
                        <Appbar.Action icon="archive" onPress={() => console.log('Pressed archive')} />
                        <Appbar.Action icon="mail" onPress={() => console.log('Pressed mail')} />
                        <Appbar.Action icon="label" onPress={() => console.log('Pressed label')} />
                        <Appbar.Action icon="delete" onPress={() => console.log('Pressed delete')} />
                    </Appbar>
                    <Checkbox
                        status={checked ? 'checked' : 'unchecked'}
                        onPress={() => { this.setState({ checked: !checked }); }}
                    />
                    <List.Section title="Accordions">
                        <List.Accordion
                            title="Uncontrolled Accordion"
                            left={props => <List.Icon {...props} icon="folder" />}
                        >
                            <List.Item title="First item" />
                            <List.Item title="Second item" />
                        </List.Accordion>

                        <List.Accordion
                            title="Controlled Accordion"
                            left={props => <List.Icon {...props} icon="folder" />}
                            expanded={this.state.expanded}
                            onPress={this._handlePress}
                        >
                            <List.Item title="First item" />
                            <List.Item title="Second item" />
                        </List.Accordion>
                    </List.Section>
                    <TouchableRipple
                        onPress={() => console.log('Pressed')}
                        rippleColor="rgba(0, 0, 0, .32)"
                    >
                        <Text>TouchableRipple</Text>
                    </TouchableRipple>
                </PaperProvider>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    bottom: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
    },
    safeArea: {
        flex: 1,
        backgroundColor: 'white',
        paddingTop: Platform.OS === 'android' ? 25 : 0
    },
});

export default PaperUiScreen

