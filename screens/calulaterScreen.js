import React from 'react';
import { StyleSheet, Text, View,TouchableOpacity,Button } from 'react-native';

export default class CalculaterScreen extends React.Component {
    static navigationOptions = {
        title :"Calculator",
    }

    constructor(){
        super()
        this.state = {
        resultText:"",
        afterCalculate:""
    }    
  }

  
  calcuateResult(){
    if (['+','-','*','/'].includes(this.state.resultText.slice(-1))) {
      return
    }
    const text = this.state.resultText
    console.log(text,eval(text));
    this.setState({
      afterCalculate: eval(text)
    })

  }

  buttonPressed(text){

    if (text == '=') {
      return this.calcuateResult()
    }

    this.setState({
      resultText:this.state.resultText+text
    })
  }

  operate(operation){   
    if (this.state.resultText == "") {
      return
    }    
    switch(operation){
      case 'D':
        let text = this.state.resultText.split('')      
        
        text.pop()        
        this.setState({
          resultText : text.join('')
        })
        break
        case '+':
        case '-':
        case '*':
        case '/':
        const lastChar = this.state.resultText.slice(-1);
        if (['+','-','*','/'].includes(lastChar)) {
          return          
        }
        this.setState({
          resultText : this.state.resultText+operation
        })
    }
  }
  
  render() {
    let rows = []
    let nums= [[1,2,3],[4,5,6],[7,8,9],['.',0,'=']]
    for (let i = 0; i < 4; i++) {
      let row = []
      for (let j = 0; j < 3; j++) {
        row.push(<TouchableOpacity key={nums[i][j]}onPress = {()=> this.buttonPressed(nums[i][j])} style={styles.btn}>
          <Text style={styles.btnText}>{nums[i][j]}</Text>
        </TouchableOpacity>)
      }      
      rows.push(<View key={i} style={styles.row}>{row}</View>)
    }
    let operations = ['D','+','-','*','/'];  
    let operationsText = []
    for (let i = 0; i < 5; i++) {
      operationsText.push(<TouchableOpacity key={operations[i]} onPress = {()=> this.operate(operations[i])} style={styles.btn}>
        <Text style={styles.btnText}>{operations[i]}</Text>
      </TouchableOpacity>)      
    }

    return (
      <View style={styles.container}>
        <View style={styles.result}>
          <Text style={styles.resultText}>{this.state.resultText}</Text>
        </View>
        <View style={styles.calculation}>
          <Text style={styles.calculationText}>{this.state.afterCalculate}</Text>
        </View>
        <View style={styles.buttons}>
          <View style={styles.numbers}>           
            {rows}
          </View>
          <View style={styles.operations}>
            {operationsText}
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,    
  },
  calculationText :{
    fontSize:30,
    color:'black'
  },
  resultText:{
    fontSize:30,
    color:'black'
  },
  result :{
    flex :2,
    backgroundColor : 'white',
    justifyContent : 'center',
    alignItems : 'flex-end'
  },
  calculation :{
    flex :1,
    backgroundColor : 'white',
    justifyContent : 'center',
    alignItems : 'flex-end'
  },
  buttons :{
    flex:7,
    flexDirection:'row'
  },
  numbers :{
    flex :3,
    backgroundColor : '#444'
  },
  row :{
    flexDirection : 'row',
    flex:1,
    justifyContent : 'space-around',
    alignItems:'center'
  },
  operations :{
    flex:1,
    justifyContent : 'space-around',
    alignItems:'stretch',
    backgroundColor : '#636363'
  },
  btn :{
    flex :1,
    alignItems:'center',
    justifyContent:'center',
    alignSelf:'stretch',
  },
  btnText :{
    fontSize:30,
    color:'white'
  },
});
