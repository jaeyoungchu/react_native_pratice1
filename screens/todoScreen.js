import React, { Componet } from 'react'
import { View, Text, StyleSheet, TextInput, Dimensions, Platform, ScrollView } from 'react-native'
import ToDo from '../components/toDo';
const { height, width } = Dimensions.get("window")

class ToDoScreen extends React.Component {
    state = {
        newToDo: ""
    }
    render() {
        const { newToDo } = this.state

        return (
            <View style={styles.container}>
                <Text style={styles.title}>To Do</Text>
                <View style={styles.card}>
                    <TextInput
                        style={styles.input}
                        placeholder={"New To Do"}
                        placeholderTextColor={"#999"}
                        value={newToDo}
                        onChangeText={this._controllNewToDo}
                        returnKeyType={"done"}
                        autoCorrect={false}
                    />
                    <ScrollView>
                        <ToDo />
                    </ScrollView>

                </View>
            </View>
        )
    }

    _controllNewToDo = text => {
        this.setState({
            newToDo: text
        })
    }

}

export default ToDoScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 25,
        backgroundColor: "#F23547",
        alignItems: 'center',
    },
    title: {
        color: 'white',
        fontSize: 30,
        marginTop: 50,
        fontWeight: '200',
        marginBottom: 30,
    },
    card: {
        backgroundColor: 'white',
        flex: 1,
        width: width - 25,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        ...Platform.select({
            ios: {
                shadowColor: 'rgb(50,50,50)',
                shadowOpacity: 0.5,
                shadowRadius: 5,
                shadowOffset: {
                    height: -1,
                    width: 0
                },
            },
            android: {
                elevation: 5
            }
        })
    },
    input: {
        padding: 20,
        borderBottomColor: '#bbb',
        borderBottomWidth: 1,
        fontSize: 25,
    },
});