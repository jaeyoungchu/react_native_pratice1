import React, { Componet } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Dimensions } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';

const { width, height } = Dimensions.get("window")

class ToDo extends React.Component {
    state = {
        isEditing: false,
        isCompleted: false,
    }

    render() {
        const { isCompleted, isEditing } = this.state
        console.log(isEditing);

        return (
            <View style={styles.container}>
                <View style={styles.column}>
                    <TouchableOpacity onPress={this._toggleComplete}>
                        <View style={[styles.circle, isCompleted ? styles.completedCircle : styles.uncompletedCircle]}>

                        </View>
                    </TouchableOpacity>
                    <Text style={[styles.text, isCompleted ? styles.completedText : styles.uncompletedText]}>Hello I'm ToDo</Text>
                </View>

                    {isEditing ? (
                        <View style={styles.actions}>
                            <TouchableOpacity onPress={_ => this._finishEditing()}>
                                <View>
                                    <Icon name="check-square" size={30} color="green"></Icon>
                                </View>
                            </TouchableOpacity>
                        </View>
                    ) : (
                            <View style={styles.actions}>
                                <TouchableOpacity onPress={_ => this._startEditing()}>
                                    <View style={styles.actionContainer}>
                                        <Icon style={styles.actionText} name="pencil" size={30} color="green"></Icon>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <View style={styles.actionContainer}>
                                        <Icon style={styles.actionText} name="window-close" size={30} color="red"></Icon>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        )}
                
            </View>
        )
    }

    _toggleComplete = () => {
        this.setState(prevState => {
            return {
                isCompleted: !prevState.isCompleted
            }
        })
    }

    _startEditing() {
        console.log('start editing');

        this.setState({
            isEditing: true
        })
    }
    _finishEditing() {
        this.setState({
            isEditing: false
        })
    }
}

export default ToDo

const styles = StyleSheet.create({
    container: {
        width: width - 50,
        borderBottomColor: '#bbb',
        borderBottomWidth: StyleSheet.hairlineWidth,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    text: {
        fontWeight: '600',
        fontSize: 20,
        marginVertical: 20,
    },
    circle: {
        width: 40,
        height: 40,
        borderRadius: 20,
        borderColor: "red",
        borderWidth: 3,
        marginRight: 10,
        marginLeft: 5,
    },
    completedCircle: {
        borderColor: "#bbb"
    },
    uncompletedCircle: {
        borderColor: "#F23657"
    },
    completedText: {
        color: "#bbb",
        textDecorationLine: "line-through"
    },
    uncompletedText: {
        color: "#353839"
    },
    column: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: width / 2,
    },
    actions: {
        flexDirection: 'row',
    },
    actionContainer: {
        margin: 2,
    }
});