import React, {Componet} from 'react'
import {View,Text,TouchableOpacity,SafeAreaView,Image} from 'react-native'
 
class SideBar extends React.Component{
    navigate(route){
        this.props.navigation.navigate(route)
    }
    render() { 
        const routes=[{
            title:"home",
            route:"Home"
        },{
            title:"calculater",
            route:"Calculater"
        },{
            title:"LoginExample",
            route:"LoginEx"
        }]
        return(
            <SafeAreaView>
                <Image style={{width:250,height:300}}
                    source={{uri:'http://images.clipartpanda.com/clarinet-clipart-A_Treble_Clef_and_a_Clarinet_100215-214565-762009.jpg'}}
                />
               {
                   routes.map(  e=> (
                       <TouchableOpacity key={e.title} style={{justifyContent:'center',marginBottom:20, borderBottomWidth:1,borderBottomColor:'grey'}}
                       onPress={_=> this.navigate(e.route)}>
                        <Text>{e.title}</Text>
                       </TouchableOpacity>
                   )

                   )
               }
            </SafeAreaView>
        )
    }
}

export default SideBar