import {StyleSheet} from 'react-native'

export default styles = StyleSheet.create({
    
    heading : {
        fontSize:25,
        textAlign:'center',
    },
    input : {
        marginLeft: 20,
        marginRight : 20,
        borderColor: 'gray',
        borderWidth: 1,
    },  
    button:{
        marginLeft: 20,
        marginRight : 20,
    },
    parent : {
        flex:1,
        justifyContent:'center'
    },
          
});