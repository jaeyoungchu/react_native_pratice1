import {createStackNavigator,createAppContainer,createDrawerNavigator} from 'react-navigation'
import HomeScreen from './screens/homeScreen'
import CalculaterScreen from './screens/calulaterScreen'
import LoginExScreen from './screens/loginExScreen'
import PaperUiScreen from './screens/paperuiScreen'
import ToDoScreen from './screens/todoScreen'
import SideBar from './components/sideBar'

// const {width} = Dimensions.get('window');

const AppNavigator = createDrawerNavigator({
  Home : HomeScreen,
  Calculater : CalculaterScreen,
  LoginEx : LoginExScreen,
  PaperEx : PaperUiScreen,
  ToDoEx : ToDoScreen,
},{
  contentComponent : SideBar,
    // drawerWidth : width/2,
    contentOptions:{
      activeTintColor:'red'
    }
})

const App = createAppContainer(AppNavigator);

export default App;

